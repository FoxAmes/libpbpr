#[derive(Debug)]
pub struct PSARHeader {
    pub magic: u32,
    pub flags: u32,
    pub length: u32,
    pub a_type: u32,
}
