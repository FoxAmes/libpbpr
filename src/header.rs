use std::{
    fs::File,
    io::{Read, Seek, SeekFrom},
    mem::size_of,
};

pub const MAGIC: [u8; 4] = *b"PBP\0";

pub enum PBPFlag {
    UPDATER = 0b1 << 16, // Only flag present in the updater
}

#[allow(dead_code)]
/// Struct representing the data in a PBP header.
///
/// # Members
///
/// `magic`: a 4-byte string at the beginning of the file. Always `b"PBP\0"`.
///
/// `flags`: a 32-bit collection of flags.
///
/// `metadata_offset`: the offset of the PBP metadata.
///
/// `thumbnail_offset`: the offset of the PNG thumbnail.
///
/// `code_offsets`: several offsets to code entrypoints.
///
/// `data_offset`: offset to a PSAR containing data for the PBP.
#[derive(Debug)]
pub struct PBPHeader {
    pub magic: [u8; 4],
    pub flags: u32,
    pub metadata_offset: u32,
    pub thumbnail_offset: u32,
    pub code_offsets: [u32; 5], // Unknown why there are multiple offsets at this time
    pub data_offset: u32,       // Points to a PSAR in the updater, at least
}

impl PBPHeader {
    /// Attempt to populate a `PBPHeader` from a given file.
    ///
    /// Seeks to the beginning of the file, attempts to parse the header,
    /// then returns to the original position.
    ///
    /// Returns either a populated `PBPHeader`, or an error.
    ///
    /// # Errors
    ///
    /// Will fail if the file magic doesn't match.
    ///
    /// Will fail if unable to read the file.
    pub fn from_file(f: &mut File) -> Result<Self, String> {
        // Save stream position before returning to beginning of file
        let f_pos = match f.stream_position() {
            Ok(p) => p,
            Err(e) => return Err(format!("Unable to save file stream position: {}", e)),
        };
        match f.seek(SeekFrom::Start(0)) {
            Ok(_) => {}
            Err(e) => return Err(format!("Unable to seek to beginning of file: {}", e)),
        }

        const H_LEN: usize = size_of::<PBPHeader>();
        let mut f_header: [u8; H_LEN] = [0; H_LEN];

        match f.read(&mut f_header) {
            Ok(b) => {
                match f.seek(SeekFrom::Start(f_pos)) {
                    Ok(_) => {}
                    Err(e) => return Err(format!("Unable to restore file position: {}", e)),
                }
                match b {
                    H_LEN => return Self::parse(&f_header),
                    _ => {
                        return Err("File is too short.".to_string());
                    }
                }
            }
            Err(e) => {
                match f.seek(SeekFrom::Start(f_pos)) {
                    Ok(_) => {}
                    Err(e) => return Err(format!("Unable to restore file position: {}", e)),
                }
                return Err(format!("Unable to read from file: {}", e));
            }
        };
    }

    /// Parse a `PBPHeader` from a given byte array.
    /// Byte array must start with PBP magic and be exactly `size_of::<PBPHeader>()` bytes long.
    /// Bytes must be in little-endian order.
    pub fn parse(header: &[u8; size_of::<Self>()]) -> Result<Self, String> {
        // Check for PBP magic
        let mut magic = [0; 4];
        magic.clone_from_slice(&header[0..4]);
        magic.reverse();
        if magic != MAGIC {
            return Err("Header magic does not match.".to_string());
        }

        // Read flags and offsets
        let flags = u32::from_le_bytes(
            header[4..8]
                .try_into()
                .expect("slice with incorrect length"),
        );
        let metadata_offset = u32::from_le_bytes(
            header[8..12]
                .try_into()
                .expect("slice with incorrect length"),
        );
        let thumbnail_offset = u32::from_le_bytes(
            header[12..16]
                .try_into()
                .expect("slice with incorrect length"),
        );
        let mut i = 0;
        let code_offsets = [0; 5].map(|_| {
            let t = u32::from_le_bytes(
                header[(16 + 4 * i)..(20 + 4 * i)]
                    .try_into()
                    .expect("slice with incorrect length"),
            );
            i += 1;
            t
        });
        i -= 1;
        let data_offset = u32::from_le_bytes(
            header[(20 + 4 * i)..(24 + 4 * i)]
                .try_into()
                .expect("slice with incorrect length"),
        );

        Ok(Self {
            magic,
            flags,
            metadata_offset,
            thumbnail_offset,
            code_offsets,
            data_offset,
        })
    }
}
